import serial


class CommunicationChannelUART:
    ser = serial

    def init(self, file_path, baud_rate):
        self.ser = serial.Serial(file_path, baud_rate)

    def write(self, data):
        self.ser.write(data)

    def read(self, read_len):
        return self.ser.read(read_len)

    def serial_is_open(self):
        return self.ser.is_open

    def close(self):
        self.ser.close()