import sensor_config
import tag_config


class SensorNotificationInfo:
    sensor_id = 0
    enable_status = False
    last_updated_time = 0
    sensor_config = sensor_config.SensorConfig()


class TagConnectedInfo:
    tag_connection_handle = 0
    tag_config_info = tag_config.TagConfig()
    tag_id = []
    is_notifications_enabled = False
    sensor_notification_info = {}

    def add_sensor_notification_info(self, sensor_obj):
        if sensor_obj is None:
            return
        self.sensor_notification_info[sensor_obj.sensor_id] = sensor_obj

    def get_sensor_notification_info(self, sensor_id):
        return self.sensor_notification_info.get(sensor_id)

    def pop_sensor_notification(self, sensor_id):
        return self.sensor_notification_info.pop(sensor_id)
