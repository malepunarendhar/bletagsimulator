import data_store
import time


def tag_config_obj_process(row):
    tag_config_data = data_store.tags_config_master.get(row[0])
    if tag_config_data is None :
        data_store.tags_config_master[row[0]] = {}
        data_store.tags_config_master[row[0]]['tag_id'] = row[0]
        data_store.tags_config_master[row[0]]['last_report_time'] = time.time()
        data_store.tags_config_master[row[0]]['tag_type_id'] = row[1]
        tag_type_obj = data_store.tag_types_config_master.get(row[1])
        if tag_type_obj :
            data_store.tags_config_master[row[0]]['adv_interval'] = data_store.tag_types_config_master[row[1]].get('adv_interval')
        else :
            del data_store.tags_config_master[row[0]]

