import array as arr
import command_packets
import threading
import data_store
import time
# import communication_channel_uart
import communication_channel_tcp
import logging

scan_status = False
scan_mutex = threading.Lock()
post_io_mutex = threading.Lock()
communication_channel_tcp_obj = communication_channel_tcp.CommunicationChannelTCP()
# communication_channel_uart_obj = communication_channel_uart.CommunicationChannelUART()


def adv_simulator():
    logging.info("adv simulator started")
    result_arr = arr.array('B', [0, 0x8a])
    len_s = arr.array('B', [0, 0x8a])
    #    communication_channel_uart_obj.init("/dev/ttyUSB0", 115200)
    communication_channel_tcp_obj.init("192.168.7.1", 32325)
    adv_thread = threading.Thread(target=process_adv_packets)
    adv_thread.start()
    while True:
        client_address = communication_channel_tcp_obj.listen()
        try:
            logging.info(client_address)
            while True:
                result_arr *= 0
                len_s *= 0
                len_s = communication_channel_tcp_obj.read(2)
                read_len = int(len_s[0]) + int(len_s[1])
                result_arr = communication_channel_tcp_obj.read(read_len)
                if result_arr[1] == 0x69:
                    post_io_packets(command_packets.param_update_resp_packet)
                elif result_arr[1] == 0x60:
                    post_io_packets(command_packets.stack_enable_resp_packet)
                elif result_arr[1] == 0x77:
                    post_io_packets(command_packets.tx_power_resp_packet)
                elif result_arr[1] == 0x8a:
                    post_io_packets(command_packets.start_scan_resp_packet)
                    start_scan()
                elif result_arr[1] == 0x8b:
                    post_io_packets(command_packets.stop_scan_resp_packet)
                    stop_scan()
                else:
                    logging.warning("Invalid data")
        finally:
            communication_channel_tcp_obj.connection_close()


def stop_scan():
    global scan_status
    global scan_mutex
    scan_mutex.acquire()
    scan_status = False
    scan_mutex.release()
    logging.info("scan stopped")


def post_io_packets(data):
    """
    print("adv simulate start")
    sam_index = 11
    for tag_id, tag_info in data_store.data_store_obj.tags_config_master.items():
        for count in range(0, 6):
            command_packets.data_push_scan[sam_index - count] = tag_info[0][count]
        for data_count in range(0, len(command_packets.data_push_scan)):
            print(hex(command_packets.data_push_scan[data_count]), end=" "),
        print(" ")
"""
    global post_io_mutex
    if data is None:
        return
    data_len = len(data)
    data_len_arr = data_len.to_bytes(2, byteorder='little')
    
    """
    print(hex(data_len_arr[0]), end=" "),
    print(hex(data_len_arr[1]), end=" "),
    for data_count in range(0, len(data)):
        print(hex(data[data_count]), end=" "),
    print(" ")
    
    """
    post_io_mutex.acquire()
    communication_channel_tcp_obj.write(data_len_arr)
    communication_channel_tcp_obj.write(data)
    post_io_mutex.release()


def start_scan():
    global scan_status
    global scan_mutex
    scan_mutex.acquire()
    scan_status = True
    scan_mutex.release()
    logging.info("scan started")


def process_adv_packets():
    global scan_status
    while True:
        if not scan_status:
            continue
        for key in data_store.tags_config_master:
            tag_obj = data_store.tags_config_master.get(key)
            if (time.time() - tag_obj.get('last_report_time')) >= int(tag_obj.get('adv_interval')):
                tag_obj['last_report_time'] = time.time()
                sam_index = 11
                mac_id = bytes.fromhex(key)
                data_push_scan_resp = arr.array('B', [])
                data_push = arr.array('B', [])
                data_push_scan_resp.extend(command_packets.data_push_scan_response)
                data_push.extend(command_packets.data_push_scan)
                print(key)
                for count in range(0, 6):
                    data_push_scan_resp[sam_index - count] = mac_id[count]
                    data_push[sam_index - count] = mac_id[count]
                logging.debug(key)
                post_io_packets(data_push)
                post_io_packets(data_push_scan_resp)
