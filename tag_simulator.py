import csv
import tag_type_config
import tag_config
import logging
import data_store
import threading
import adv_simulator
import connect_simulator


def main():
    logging.basicConfig(level=logging.INFO,
                        format='%(created)f %(levelname)s [%(filename)s->%(funcName)s:%(lineno)d] %(message)s')
    rows = []
    logging.info("main entry")
    with open('sample_tag_types.csv') as csv_file:
        tag_type_csv_reader = csv.reader(csv_file)
        next(tag_type_csv_reader)
        for row in tag_type_csv_reader:
            rows.append(row)
    for row in rows[:tag_type_csv_reader.line_num]:
        if not row[0]:
            continue
        tag_type_config.tag_type_config_obj_process(row)

    print(data_store.tag_types_config_master)
    rows = []
    with open('sample_tag_ids.csv') as csv_file:
        tag_csv_reader = csv.reader(csv_file)
        for row in tag_csv_reader:
            rows.append(row)
    for row in rows[:tag_csv_reader.line_num]:
        if not row[0]:
            continue
        tag_config.tag_config_obj_process(row)

    adv_simulator_thread = threading.Thread(target=adv_simulator.adv_simulator)
    adv_simulator_thread.start()
    connect_simulator_thread = threading.Thread(target=connect_simulator.connect_simulator)
    connect_simulator_thread.start()
    while True:
        hold = True

    logging.info("main exit")


if __name__ == "__main__":
    main()
