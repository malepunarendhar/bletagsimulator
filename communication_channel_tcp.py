import socket


class CommunicationChannelTCP:
    sock = socket
    connection = None
    client_address = None

    def init(self, server_ip, port):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = (server_ip, port)
        # print >> sys.stderr, 'starting up on %s port %s' % server_address
        self.sock.bind(server_address)

    def listen(self):
        self.sock.listen(1)
        # Wait for a connection
        # print >> sys.stderr, 'waiting for a connection'
        self.connection, self.client_address = self.sock.accept()
        return self.client_address

    def read(self, read_len):
        read_data = []
        while True:
            data = self.connection.recv(read_len)
            len_read = len(data)
            if len_read <= read_len:
                read_data.extend(data)
                if read_len == len_read:
                    break
                else:
                    read_len = read_len - len_read
            else:
                break
        return read_data

    def write(self, data):
        self.connection.sendall(data)

    def connection_close(self):
        self.connection.close()

    def close(self):
        self.sock.close()
