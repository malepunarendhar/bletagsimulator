import sensor_config
import data_store


def tag_type_config_obj_process(row):
    tag_type_data = data_store.tag_types_config_master.get(row[0])
    if tag_type_data is None :
        data_store.tag_types_config_master[row[0]] = {}
        data_store.tag_types_config_master[row[0]]['tag_type_id'] = row[0]
        data_store.tag_types_config_master[row[0]]['adv_interval'] = row[1]
    data_store.tag_types_config_master[row[0]][row[8]] = {}
    data_store.tag_types_config_master[row[0]][row[8]]['sensor_id'] = row[2]
    data_store.tag_types_config_master[row[0]][row[8]]['name'] = row[3]
    data_store.tag_types_config_master[row[0]][row[8]]['status'] = row[4]
    data_store.tag_types_config_master[row[0]][row[8]]['operation'] = row[5]
    data_store.tag_types_config_master[row[0]][row[8]]['read_mode'] = row[6]
    data_store.tag_types_config_master[row[0]][row[8]]['report_interval'] = row[7]
    data_store.tag_types_config_master[row[0]][row[8]]['handle'] = row[8]
    data_store.tag_types_config_master[row[0]][row[8]]['data_value_type'] = row[9]
    data_store.tag_types_config_master[row[0]][row[8]]['data_value_min'] = row[10]
    data_store.tag_types_config_master[row[0]][row[8]]['data_value_max'] = row[11]
