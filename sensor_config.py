class SensorConfig:
    sensor_id = "0"
    name = " "
    status = "enabled"
    operation = "unknown"
    read_mode = "unknown"
    report_interval = "0"
    handle = "0"
    data_value_type = "fixed"
    data_value_min = "0"
    data_value_max = "0"


def sensor_config_obj_process(row):
    sensor_obj = SensorConfig()
    sensor_obj.sensor_id = row[2]
    sensor_obj.name = row[3]
    sensor_obj.status = row[4]
    sensor_obj.operation = row[5]
    sensor_obj.read_mode = row[6]
    sensor_obj.report_interval = row[7]
    sensor_obj.handle = row[8]
    sensor_obj.data_value_type = row[9]
    sensor_obj.data_value_min = row[10]
    sensor_obj.data_value_max = row[11]
    return sensor_obj
