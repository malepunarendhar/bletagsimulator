import array as arr
import command_packets
import threading
import data_store
import random
import time
# import communication_channel_uart
import communication_channel_tcp
import logging

connect_event_packet_mutex = threading.Lock()
connect_event_packets = []
connect_post_io_mutex = threading.Lock()
# communication_channel_uart_obj = communication_channel_uart.CommunicationChannelUART()
communication_channel_tcp_obj = communication_channel_tcp.CommunicationChannelTCP()


def connect_simulator():
    global connect_event_packets
    global connect_event_packet_mutex
    logging.info("connect simulator started")
    result_arr = arr.array('B', [0, 0x8a])
    len_s = arr.array('B', [0, 0x8a])
    communication_channel_tcp_obj.init("192.168.7.1", 32326)
    connect_thread = threading.Thread(target=process_connect_packets)
    connect_thread.start()
    while True:
        client_address = communication_channel_tcp_obj.listen()
        try:
            logging.info(client_address)
            while True:
                result_arr *= 0
                len_s *= 0
                len_s = communication_channel_tcp_obj.read(2)
                read_len = int(len_s[0]) + int(len_s[1])
                result_arr = communication_channel_tcp_obj.read(read_len)
                if result_arr[1] == 0x69:
                    post_io_packets(command_packets.param_update_resp_packet)
                elif result_arr[1] == 0x60:
                    post_io_packets(command_packets.stack_enable_resp_packet)
                elif result_arr[1] == 0x77:
                    post_io_packets(command_packets.tx_power_resp_packet)
                elif result_arr[1] == 0x8c:
                    post_io_packets(command_packets.connect_resp_packet)
                    connect_event_packet_mutex.acquire()
                    connect_event_packets.append(list(result_arr))
                    connect_event_packet_mutex.release()
                elif result_arr[1] == 0x76:
                    post_io_packets(command_packets.disconnect_resp_packet)
                    connect_event_packet_mutex.acquire()
                    connect_event_packets.append(list(result_arr))
                    connect_event_packet_mutex.release()
                elif result_arr[1] == 0x9c:
                    post_io_packets(command_packets.write_resp_packet)
                    connect_event_packet_mutex.acquire()
                    connect_event_packets.append(list(result_arr))
                    connect_event_packet_mutex.release()
                elif result_arr[1] == 0x9a:
                    post_io_packets(command_packets.read_resp_packet)
                    connect_event_packet_mutex.acquire()
                    connect_event_packets.append(list(result_arr))
                    connect_event_packet_mutex.release()
                else:
                    logging.warning("Invalid data: ")
                    for count in range(0, len(result_arr)):
                        print(hex(result_arr[count]), end=" "),
                    print(" ")
        finally:
            communication_channel_tcp_obj.connection_close()


def process_connect_packets():
    global connect_event_packets
    global connect_event_packet_mutex
    print("process_connect_packets started")
    while True:
        data_frame = None
        connect_event_packet_mutex.acquire()
        if len(connect_event_packets):
            data_frame = connect_event_packets.pop(0)
        connect_event_packet_mutex.release()
        if data_frame:
            if data_frame[1] == 0x8c:
                # print("process_connect_packets")
                # print(data_frame)
                connect(data_frame)
            elif data_frame[1] == 0x76:
                disconnect(data_frame)
            elif data_frame[1] == 0x9c:
                write(data_frame)
            elif data_frame[1] == 0x9a:
                read(data_frame)
        tags_connected = data_store.tags_connected_master
        for key, value in tags_connected.items():
            hvx_data(value)


def post_io_packets(data):
    global connect_post_io_mutex
    if data is None:
        return
    data_len = len(data)
    data_len_arr = data_len.to_bytes(2, byteorder='little')
    """
    print(hex(data_len_arr[0]), end=" "),
    print(hex(data_len_arr[1]), end=" "),
    for data_count in range(0, len(data)):
        print(hex(data[data_count]), end=" "),
    print(" ")
    """
    connect_post_io_mutex.acquire()
    communication_channel_tcp_obj.write(data_len_arr)
    communication_channel_tcp_obj.write(data)
    connect_post_io_mutex.release()


def connect(data):
    mac_id = data[4:10]
    mac_id.reverse()
    tag_lower_id = str(''.join(format(x, '02x') for x in mac_id))
    tag_id = tag_lower_id.upper()
    logging.info("Node [" + tag_id + "] connect request processing")
    tag_config_obj = data_store.tags_config_master.get(tag_id)
    if not tag_config_obj:
        logging.warning("Invalid Node [" + tag_id + "]")
        return
    conn_handle = 0
    while conn_handle < 20:
        tag_connected_obj = data_store.tags_connected_master.get(conn_handle)
        if tag_connected_obj is None:
            break
        conn_handle += 1
    if conn_handle == 20:
        logging.warning("Invalid connections count " + str(conn_handle))
        return
    data_store.tags_connected_master[conn_handle] = {}
    data_store.tags_connected_master[conn_handle]['conn_handle'] = conn_handle
    data_store.tags_connected_master[conn_handle]['tag_id'] = tag_id
    tag_config_obj = data_store.tags_config_master.get(tag_id)
    data_store.tags_connected_master[conn_handle]['tag_type_id'] = tag_config_obj.get('tag_type_id')
    connect_data = command_packets.connected_resp_packet
    connect_data[3] = conn_handle
    sam_index = 11
    for count in range(0, 6):
        connect_data[sam_index - count] = mac_id[count]
    logging.info("Node [" + str(tag_id) + "] handle " + str(conn_handle) + " connected response posting")
    post_io_packets(connect_data)


def disconnect(data):
    connection_handle = data[2]
    tag_connected_obj = data_store.tags_connected_master.get(connection_handle)
    if tag_connected_obj is None:
        logging.warning("Invalid connection handle " + str(connection_handle))
        return
    logging.info("Node [" + tag_connected_obj.get('tag_id') + "] disconnect request processing")
    disconnect_data = command_packets.disconnected_resp_packet
    disconnect_data[3] = connection_handle
    logging.info("Node [" + tag_connected_obj.get('tag_id') + "] disconnected response posting")
    post_io_packets(disconnect_data)
    data_store.tags_connected_master.pop(connection_handle)


def read(data):
    connection_handle = data[2]
    read_handle = data[4]
    tag_connected_obj = data_store.tags_connected_master.get(connection_handle)
    if tag_connected_obj is None:
        logging.warning("Invalid connection handle " + str(connection_handle))
        return
    logging.info("Node [" + tag_connected_obj.get('tag_id') + "] handle " + str(read_handle) +
                 " read request processing")
    tag_type_id = tag_connected_obj.get('tag_type_id')
    tag_type_obj = data_store.tag_types_config_master.get(tag_type_id)
    handle = tag_type_obj.get(str(read_handle))
    if handle is None:
        logging.info(
            "Node [" + tag_connected_obj.get('tag_id') + "] handle " + str(read_handle) + " Invalid")
        return False
    read_data = command_packets.read_event_resp_packet
    read_data[3] = connection_handle
    read_data[9] = read_handle
    if handle.get('data_value_type') == "Fixed":
        read_data[13] = len(handle.get('data_value_min'))
        read_data.extend(handle.get('data_value_min'))
    elif handle.get('data_value_type') == "Variable":
        sam_index = 15
        min_data = int(handle['data_value_min'])
        max_data = int(handle['data_value_max'])
        rand_data_int = int(random.randrange(min_data, max_data))
        rand_data = rand_data_int.to_bytes((rand_data_int.bit_length() + 7) // 8, byteorder='little')
        rand_data_len = len(rand_data)
        rand_data_len_arr = rand_data_len.to_bytes(2, byteorder='little')
        read_data[13] = rand_data_len_arr[0]
        read_data[14] = rand_data_len_arr[1]
        count = 0
        while count < len(rand_data):
            read_data[count + sam_index] = rand_data[count]
            count += 1
    logging.info("Node [" + tag_connected_obj.get('tag_id') + "] sensor " + handle['name'] +
                 " read response posting")
    post_io_packets(read_data)


def write(data):
    connection_handle = data[2]
    write_handle = data[7]
    tag_connected_obj = data_store.tags_connected_master.get(connection_handle)
    if tag_connected_obj is None:
        logging.warning("Invalid connection handle " + str(connection_handle))
        return
    logging.info("Node [" + tag_connected_obj.get('tag_id') + "] handle " + str(write_handle) +
                 " write request processing")
    tag_type_id = tag_connected_obj.get('tag_type_id')
    tag_type_obj = data_store.tag_types_config_master.get(tag_type_id)
    handle = tag_type_obj.get(str(write_handle))
    notification_handle = tag_type_obj.get(str(write_handle + 1))
    if handle:
        logging.info("Node [" + tag_connected_obj.get('tag_id') + "] sensor " +
                     notification_handle.get('name') + " write operation is done")
        return True
    elif notification_handle:
        noti_handle = notification_handle['handle']
        if notification_handle.get('operation') == "Notification":
            write_data = command_packets.write_event_resp_packet
            write_data[3] = connection_handle
            write_data[9] = int(noti_handle) - 1
            sensor_notification_obj = tag_connected_obj.get(str(write_handle + 1))
            if sensor_notification_obj:
                if int(data[14]) == 0:
                    tag_connected_obj.pop(noti_handle)
                    post_io_packets(write_data)
                    logging.info("Node [" + tag_connected_obj.get('tag_id') + "] sensor " +
                                 notification_handle.get('name') + " removing sensor from notification")
                    return True
            tag_connected_obj['is_notifications_enabled'] = True
            tag_connected_obj[noti_handle] = {}
            tag_connected_obj[noti_handle]['sensor_id'] = notification_handle.get('sensor_id')
            tag_connected_obj[noti_handle]['enable_status'] = True
            tag_connected_obj[noti_handle]['sensor_config'] = noti_handle
            tag_connected_obj[noti_handle]['last_updated_time'] = 0
            logging.info("Node [" + tag_connected_obj.get('tag_id') + "] sensor " +
                         notification_handle.get('name') + " added sensor to notification")
        else:
            logging.warning("Node [" + tag_connected_obj.get('tag_id') + "] Invalid handle " + str(write_handle) +
                            " notification request processing")
            return False
    else:
        logging.warning("Node [" + tag_connected_obj.get('tag_id') + "] Invalid handle " + str(write_handle) +
                        " write request processing")
        return False
    logging.info("Node [" + tag_connected_obj.get('tag_id') + "] sensor " +
                 notification_handle.get('name') + " write response posting")
    post_io_packets(write_data)


def hvx_data(tag_connected_obj):
    if tag_connected_obj is None:
        logging.warning("Invalid connection object")
        return
    if tag_connected_obj.get('is_notifications_enabled'):
        for key, value in tag_connected_obj.items():
            if isinstance(value, dict):
                if 'sensor_config' in value:
                    tag_type_id = tag_connected_obj.get('tag_type_id')
                    tag_type_obj = data_store.tag_types_config_master.get(tag_type_id)
                    sensor_config = tag_type_obj.get(key)
                    report_interval = int(sensor_config.get('report_interval'))
                    if (time.time() - value.get('last_updated_time')) >= report_interval:
                        hvx_report_data = command_packets.hvx_resp_packet
                        hvx_report_data[3] = tag_connected_obj['conn_handle']
                        hvx_report_data[9] = int(sensor_config['handle']) - 2
                        if sensor_config.get('data_value_type') == "Fixed":
                            hvx_report_data[12] = len(sensor_config.get('data_value_min'))
                            hvx_report_data.extend(sensor_config.get('data_value_min'))
                        elif sensor_config.get('data_value_type') == "Variable":
                            sam_index = 14
                            min_data = int(sensor_config.get('data_value_min'))
                            max_data = int(sensor_config.get('data_value_max'))
                            rand_data_int = int(random.randrange(min_data, max_data))
                            rand_data = rand_data_int.to_bytes((rand_data_int.bit_length() + 7) // 8,
                                                               byteorder='little')
                            rand_data_len = len(rand_data)
                            rand_data_len_arr = rand_data_len.to_bytes(2, byteorder='little')
                            hvx_report_data[12] = rand_data_len_arr[0]
                            hvx_report_data[13] = rand_data_len_arr[1]
                            count = 0
                            while count < len(rand_data):
                                hvx_report_data[count + sam_index] = rand_data[count]
                                count += 1
                        logging.info("Node [" + tag_connected_obj.get('tag_id') + "] sensor " +
                                     sensor_config.get('name') + " onhvx response posting")
                        value['last_updated_time'] = time.time()
                        post_io_packets(hvx_report_data)
